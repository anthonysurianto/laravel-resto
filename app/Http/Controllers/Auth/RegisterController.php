<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'dop' => 'required',
            'dob' => 'required',
            'address' => 'required|max:100',
            'phone_home' => 'required',
            'phone' => 'required',        
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if($data['is_rest'] == "0"){
            $user = User::create([    
                'name' => $data['name'],
                'dop' => $data['dop'],
                'dob' => date('Y-m-d',strtotime($data['dob'])),
                'address' => $data['address'],
                'phone_home' => $data['phone_home'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'active' => 1,
                'password' => Hash::make($data['password']),
            ]);
            $user
                ->roles()
                ->attach(Role::where('name', 'user')->first());
                return $user;
        }
        else{
            $user = User::create([    
                'name' => $data['name'],
                'dop' => $data['dop'],
                'dob' => date('Y-m-d',strtotime($data['dob'])),
                'address' => $data['address'],
                'phone_home' => $data['phone_home'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'active' => 1,
                'password' => Hash::make($data['password']),
                'rest_category_id'=> $data['rest_category'],
                'rest_desc'=> $data['rest_desc'],
                'rest_open'=> $data['rest_open'],
                'rest_closed'=> $data['rest_closed']
            ]);
            $user
                ->roles()
                ->attach(Role::where('name', 'restaurant')->first());
                return $user;
        }
    }

    // public function register(Request $request)
    // {
    //     $this->validator($request->all())->validate();

    //     event(new Registered($user = $this->create($request->all())));

    //     return redirect('/');
    // }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);
        $slider = [];
        $data = DB::table('banner')->get();
        foreach($data as $key=>$value){
            $slider[$key] =  $value->name;
        }
        return view('auth.login')->with('slider',$slider);
    }

    public function registerResto(Request $request){
        $data = (object) array('resto_category');
        $data->resto_category = DB::table('resto_category')->select()->get();

        return view('auth.register_resto')->with('data',$data); 
    }
}
