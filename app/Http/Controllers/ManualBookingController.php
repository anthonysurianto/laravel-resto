<?php

namespace App\Http\Controllers;

use App\User;
use App\FormDana as FormDana;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ManualBookingController extends Controller
{

    protected $notification;

    public function __construct(NotificationController $notification)
    {
      $this->middleware('auth');
      $this->notification = $notification;
      parent::__construct();
    }

    public function add(Request $request){
        $request->user()->authorizeRoles(['restaurant']);
        $user = $request->user();
        $data = (object) array('resto');

        $data->resto = DB::select('
             select 
                a.*
            from users a
            left join role_user b on a.id = b.user_id
            where b.role_id = 3 and a.id = :id
            ',['id'=>$user->id]);
        $data->resto = $data->resto[0];
        return view('booking.manualAdd')->with('data',$data);
    }

    public function create(Request $request){
        $request->user()->authorizeRoles(['restaurant']);
        $user = $request->user();

        //Save to db    
        $id = DB::table('booking')->insertGetId(
            [   'user_id' => 0, 
                'rest_id' => $user->id,
                'date_book' => $request->input('date_book'),
                'time_book' => $request->input('time_book'),
                'manual_username' => $request->input('manual_username'),
                'manual_userphone' => $request->input('manual_userphone'),
                'qty' => $request->input('qty'),
                'status' => 1,
                'created_at'=>Carbon::now()->toDateTimeString(),
                'updated_at'=>Carbon::now()->toDateTimeString()
            ]
        );
        $zero = 5 - strlen((string)$id);
        
        $numberResto = '';

        for($i = 0 ;$i<$zero;$i++){
            $numberResto = $numberResto.'0';
        }
        $numberResto =  $numberResto.$id;

        $bookingId = 'R'.$user->id.$numberResto;

        DB::table('booking')->where('id',$id)->update(
            [   
                'booking_id' => $bookingId
            ]
        );
        
        return redirect('/')->with('message', 'Your book has been process !');
    }
}
