<?php

namespace App\Http\Controllers;

use App\User;
use App\FormDana as FormDana;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\BookingsExport;

class BookingController extends Controller
{

    protected $notification;
    private $excel;


    public function __construct(NotificationController $notification,Excel $excel)
    {
      $this->middleware('auth');
      $this->notification = $notification;
      $this->excel = $excel;
      parent::__construct();
    }
    
    public function index(Request $request){
        $request->user()->authorizeRoles(['restaurant','user','admin']);
        $user = $request->user();

        if( $request->user()->hasAnyRole(['admin'])){
            $data = DB::select('
                select 
                    a.*,
                    case 
                        when a.manual_username IS NULL then b.name 
                        else a.manual_username
                    end as username,
                    case 
                        when a.manual_userphone IS NULL then b.phone 
                        else a.manual_userphone
                    end as userphone,
                    c.name as rest_name
                from booking a
                left join users b on a.user_id = b.id
                left join users c on a.rest_id = c.id
                ');
        }
        else if( $request->user()->hasAnyRole(['restaurant'])){
            $data = DB::select('
                select 
                    a.*,
                    case 
                        when a.manual_username IS NULL then b.name 
                        else a.manual_username
                    end as username,
                    case 
                        when a.manual_userphone IS NULL then b.phone 
                        else a.manual_userphone
                    end as userphone
                from booking a
                left join users b on a.user_id = b.id
                where a.rest_id = :id
                ',['id'=> $request->user()->id]);
        }
        else{
            $data = DB::select('
                select 
                    a.*,
                    b.name as rest_name,
                    b.phone as rest_phone,
                    b.phone_home as rest_phone_home
                from booking a
                left join users b on a.rest_id = b.id
                where a.user_id = :id
                ',['id'=> $request->user()->id]);
        }
        return view('booking.index')->with('data',$data);
    }

    public function add(Request $request){
        $request->user()->authorizeRoles(['user']);
        $user = $request->user();
        $data = (object) array('resto','selected_resto');
        $restoId = $request->input('resto_id') == null ? 0 : $request->input('resto_id');

        $data->resto = DB::select('
             select 
                a.*
            from users a
            left join role_user b on a.id = b.user_id
            where b.role_id = 3 and a.id = :id
            ',['id'=>$restoId]);
        $data->resto = $data->resto[0];
        
        $data->selected_resto = $restoId;
        return view('booking.add')->with('data',$data);
    }

    public function create(Request $request){
        $request->user()->authorizeRoles(['user']);
        $user = $request->user();

        //Save to db    
        $id = DB::table('booking')->insertGetId(
            [   'user_id' => $user->id, 
                'rest_id' => $request->input('rest_id'),
                'date_book' => $request->input('date_book'),
                'time_book' => $request->input('time_book'),
                'qty' => $request->input('qty'),
                'status' => 1,
                'created_at'=>Carbon::now()->toDateTimeString(),
                'updated_at'=>Carbon::now()->toDateTimeString()
            ]
        );

        $zero = 5 - strlen((string)$id);
        
        $numberResto = '';

        for($i = 0 ;$i<$zero;$i++){
            $numberResto = $numberResto.'0';
        }
        $numberResto =  $numberResto.$id;

        $bookingId = 'R'.$request->input('rest_id').$numberResto;

        DB::table('booking')->where('id',$id)->update(
            [   
                'booking_id' => $bookingId
            ]
        );

        DB::table('notification_messages')->insert(
            [   
                'sender_id' => $user->id,
                'receiver_id' => $request->input('receiver'),
                'message' => $user->name." is want to book at your restaurant", 
                'created_at'=>Carbon::now()->toDateTimeString(),
                'updated_at'=>Carbon::now()->toDateTimeString()
            ]
        );
        $notif = $this->notification->addOrUpdateNotif($request);
        
        return redirect('/')->with('message', 'Your book has been process !');
    }

    public function statusBooking(Request $request,$id,$status,$receiver){
        $request->user()->authorizeRoles(['restaurant']);
        $user = $request->user();
        $messageResponse = "";

        //Save to db    
        DB::table('booking')->where('id',$id)->update(
            [   
                'status' => $status,
                'updated_at'=>Carbon::now()->toDateTimeString()
            ]
        );
        
        $message = $user->name." accept your booking !";
        if($status == 0){
            $message = $user->name." reject your booking !";            
        }
        DB::table('notification_messages')->insert(
            [   
                'sender_id' => $user->id,
                'receiver_id' => $receiver,
                'message' => $message, 
                'created_at'=>Carbon::now()->toDateTimeString(),
                'updated_at'=>Carbon::now()->toDateTimeString()
            ]
        );

        $data = DB::table('notification')->where('user_id',$receiver)->first();
        $notif = $this->notification->update($receiver,$data->count);
        
        return redirect('/')->with('message', 'Success Update Data!');
    }


    public function downloadExcelBooking(Request $request){
        return Excel::download(new BookingsExport($request->user()->id), 'list-booking.xlsx');
    }
}
