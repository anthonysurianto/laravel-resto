<?php
namespace App\Http\Controllers;

use App\User;
use App\FormDana as FormDana;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
      parent::__construct();
    }
    public function viewUser(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $user = $request->user();

        $data = DB::select('
                select 
                    a.*,
                    c.role_id,
                    d.name as role_name
                from users a
                left join role_user c on c.user_id = a.id
                left join roles d on d.id = c.role_id
                where a.id != :id
        ',['id'=>$user->id]);
        return view('admin.user.index')->with('data',$data);
    }

    public function updateUserStatus($user_id,$active)
    {
        //Save to db    
        DB::table('users')->where('id',$user_id)->update(['active'=>$active]);
        return redirect('/admin/user')->with('message', 'Success Update Data !');
    }

    public function updateUserRole($user_id,$role_id)
    {
        //Save to db    
        DB::table('role_user')->where('user_id',$user_id)->update(['role_id'=>$role_id]);
        return redirect('/admin/user')->with('message', 'Success Update Data !');
    }
}
