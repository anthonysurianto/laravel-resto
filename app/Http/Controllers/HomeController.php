<?php

namespace App\Http\Controllers;

use App\User;
use App\FormDana as FormDana;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use Hash;

class HomeController extends Controller
{
    private $excel;

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index(Request $request)
    {
        $slider = [];
        if($request->user() != null && $request->user()->hasAnyRole(['restaurant'])){         
            
            $data = null;
            return view('resto.home')->with('data',$data);
        }
        
        $data = (object) array('banner','menu');
        $data = DB::table('banner')->get();
        foreach($data as $key=>$value){
            $slider[$key] =  $value->name;
        }
        $data->banner = $slider;
        $data->menu = DB::select("
                    select 
                        a.*,
                        b.name as rest_name
                    from menu a
                    left join users b on a.rest_id = b.id
                    order by a.created_at DESC
                    Limit 3
                ");
        return view('home')->with('data',$data);
    }

    public function searchBookByBookingId(Request $request)
    { 
        $data = DB::select('
        select 
            a.*,
            case 
                when a.manual_username IS NULL then b.name 
                else a.manual_username
            end as username,
            case 
                when a.manual_userphone IS NULL then b.phone 
                else a.manual_userphone
            end as userphone
        from booking a
        left join users b on a.user_id = b.id
        where a.rest_id = :id and booking_id like :booking_id
        ',['id'=> $request->user()->id,'booking_id'=>$request->booking_id]);
        return view('resto.home')->with('data',$data);
    }


    public function viewResto(Request $request)
    {
        if($request->input('search_resto')){
            $searchString = "%".$request->input('search_resto')."%";
            $data = DB::select("
                select 
                    a.*,
                    c.name as category_name
                from users a
                left join role_user b on a.id = b.user_id
                left join resto_category c on a.rest_category_id = c.id
                where b.role_id = 3 and a.name LIKE :name
            ",['name'=> $searchString ]);
        }else{
            $data = DB::select('
            select 
                a.*,
                c.name as category_name
            from users a
            left join role_user b on a.id = b.user_id
            left join resto_category c on a.rest_category_id = c.id
            where b.role_id = 3
            ');
        }
        return view('resto.index')->with('data',$data);
    }

    public function viewRestoDetail($id)
    {
        $data = (object) array('resto','menu');
        $data->resto = DB::select('
                    select 
                        a.*,
                        c.name as category_name
                    from users a
                    left join resto_category c on a.rest_category_id = c.id
                    where a.id = :id
            ',['id'=>$id]);
        $data->resto = $data->resto[0];

        $data->menu = DB::select('
                        select 
                            a.*
                        from menu a
                        where a.rest_id = :id
                ',['id'=>$id]);
        return view('resto.detail')->with('data',$data);
    }

    public function addForm(Request $request)
    {
        $request->user()->authorizeRoles(['admin', 'user']);
        $data = DB::table('type_peminjaman')->get();
        return view('addForm')->with('data',$data);
    }

    public function detailForm(Request $request,$id)
    {
        $data = DB::table('form_dana')
                ->join('users','users.id','=','form_dana.user_id')
                ->select('form_dana.name','form_dana.dana','form_dana.dana_potongan','form_dana.terbilang','form_dana.keperluan','form_dana.cicilan','form_dana.cicilan_potongan','form_dana.tanggal_dana','form_dana.status',
                        'users.nip','users.area','users.rekening','users.bank')
                ->where('form_dana.id',$id)
                ->first();
        return view('admin.detailForm')->with('data',$data);    
    }

    public function saveForm(Request $request)
    {
        $request->user()->authorizeRoles(['admin', 'user']);
        $user = $request->user();
        $type_peminjaman = DB::table('type_peminjaman')->where('id',$request->input('limit_peminjaman'))->first();
        $limit_peminjaman = $type_peminjaman->limit_peminjaman;

        if($request->input('dana') > $limit_peminjaman){
            return back()->with('invalid','Limit peminjaman sebesar:Rp.'.number_format($limit_peminjaman,2,',','.'));
        }
        //Save to db    
        DB::table('form_dana')->insert(
            [   'user_id' => $user->id,
                'type_peminjaman_id' =>  $request->input('limit_peminjaman'),
                'name' => $request->input('name'),
                'dana' => $request->input('dana'),
                'dana_awal' => $request->input('dana'),
                'dana_potongan' => $request->input('dana'),
                'terbilang' => $request->input('terbilang'),
                'keperluan' => $request->input('keperluan'),
                'cicilan' => $request->input('cicilan'),
                'cicilan_potongan' => $request->input('cicilan'),
                'tanggal_dana' => date('Y-m-d H:i:s',strtotime($request->input('tanggal_dana'))),
                'status' => 0,
                'created_at'=>Carbon::now()->toDateTimeString(),
                'updated_at'=>Carbon::now()->toDateTimeString()
            ]
        );
        
        return redirect('/')->with('message', 'Success Insert Data !');    
    }

    public function acceptForm($id,$status,$type_form){
        $data = (object) array('id','status','type_forms','receiver','username','total_dana','tanggal_dana');
        if($type_form == 'dana'){
            $form = DB::table('form_dana')->where('id',$id)->first();
            $data->total_dana = $form->dana;
            $data->tanggal_dana = $form->tanggal_dana;
        }
        else{        
            $form = DB::table('form_pengunduran_diri')->where('id',$id)->first();
            $user = DB::table('users')->where('id',$form->user_id)->first();
            $data->username = $user->name;
        }
        $data->id = $id;
        $data->status = $status;
        $data->type_form = $type_form;
        $data->receiver = $form->user_id;
       
        return view('admin.acceptForm')->with('data',$data);
    }

    public function updateAcceptForm($id,$status,$type_form,Request $request)
    {   
        return redirect('/')->with('message', 'Success Update Data !');
    }

    
    public function updateRejectForm($id,$status,Request $request)
    {   
        
        $request->user()->authorizeRoles(['admin', 'user']);
        $user = $request->user();

        //Save to db    
        DB::table('form_dana')->where('id',$id)->update(['status'=>$status]);
        
        return redirect('/')->with('message', 'Success Update Data !');
    
    }

    public function excelReportPeminjamanForm(Request $request){
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function detailProfile(Request $request,$id)
    {
        if( $request->user()->hasAnyRole(['restaurant'])){

            $data = DB::select('
                    select 
                        a.*,
                        c.name as category_name
                    from users a
                    left join resto_category c on a.rest_category_id = c.id
                    where a.id = :id
            ',['id'=>$id]);
            return view('admin.resto.detailProfile')->with('data',$data[0]); 
        }else{
            $data = DB::select('
                    select 
                        a.*
                    from users a
                    where a.id = :id
            ',['id'=>$id]);
            return view('admin.detailProfile')->with('data',$data[0]); 
        }   
    }

    public function editProfile(Request $request,$id)
    {
        
        if( $request->user()->hasAnyRole(['restaurant'])){
            $data = (object) array('resto','resto_category');
            $data->resto = DB::table('users')->where('id',$id)->first();
            $data->resto_category = DB::table('resto_category')->select()->get();

            return view('admin.resto.editProfile')->with('data',$data); 
        }else{
            $data = (object) array('users');
            $data->users = DB::table('users')->where('id',$id)->first();
            return view('admin.editProfile')->with('data',$data);
        }   
    }

    public function updateProfile($id,Request $request)
    {   
        
        $request->user()->authorizeRoles(['admin', 'user', 'resto']);
        $user = $request->user();
        $data = $request;
        //Save to db    
        if($data['current_password'] != null){
            if(Hash::check($data['current_password'],$user->password) != 1){
                return back()->with('invalid', 'Invalid password !');
            }

            if($data['password'] == null || $data['password_confirmation'] == null){
                return back()->with('invalid', 'You must be filled the new password !');
            }

            if($data['password'] != $data['password_confirmation']){
                return back()->with('invalid', 'Password must be match !');
            }
        }
        if( $request->user()->hasAnyRole(['restaurant'])){

            $temp = [
                'name' => $data['name'],
                'address' => $data['address'],
                'phone_home' => $data['phone_home'],
                'phone' => $data['phone'],
                'rest_category_id'=> $data['rest_category'],
                'rest_desc'=> $data['rest_desc'],
                'rest_open'=> $data['rest_open'],
                'rest_closed'=> $data['rest_closed']
            ];    
        }else{
            
            $temp = [
                'name' => $data['name'],
                'address' => $data['address'],
                'phone_home' => $data['phone_home'],
                'phone' => $data['phone']
            ];    
        }
        if($data['current_password'] != null){
            $hashPass = Hash::make($data['password']);
            $temp['password'] = $hashPass;
        }
        //Save to db    
        DB::table('users')->where('id',$id)->update($temp);
        
        return redirect('/profile/'.$id)->with('status', 'Success Update Profile !');
    
    }

    public function historyForm(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $user = $request->user();
        $data = (object) array('dana','pengunduran_diri');
        $data->dana =  DB::table('form_dana')                            
                            ->join('users','users.id','=','form_dana.user_id')
                            ->select('form_dana.id','form_dana.name','users.name as user_name','users.nip','form_dana.dana_potongan as dana','form_dana.status')
                            ->whereIn('status',[1,2])->get();
        $data->pengunduran_diri =  DB::table('form_pengunduran_diri')                            
                            ->join('users','users.id','=','form_pengunduran_diri.user_id')
                            ->select('form_pengunduran_diri.id','form_pengunduran_diri.name','users.name as user_name','users.nip','form_pengunduran_diri.status')
                            ->whereIn('status',[1,2])
                            ->get();
        return view('admin.history')->with('data',$data);
    }

    public function show(){
        return view('welcome');
    }
}
