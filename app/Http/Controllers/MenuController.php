<?php

namespace App\Http\Controllers;

use App\User;
use App\FormDana as FormDana;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
      parent::__construct();
    }

    public function index(Request $request)
    {    
        $request->user()->authorizeRoles(['restaurant']);
        $data = DB::table('menu')->where('rest_id',$request->user()->id)->get();
        return view('menu.index')->with('data',$data);
    }

    
    public function add(Request $request)
    {    
        $request->user()->authorizeRoles(['restaurant']);
        return view('menu.add');
    }

    
    public function insert(Request $request)
    {    
        $request->user()->authorizeRoles(['restaurant']);
        $storagePath = Storage::put('/menu//'.$request->user()->id.'/',  $request->file('banner'));
        $storageName = basename($storagePath);
        DB::table('menu')->insert([
            'rest_id'=>$request->user()->id,
            'name'=>$request['name'],
            'image'=>$storageName,
            'price'=>$request['price'],
            'description'=>$request['description'],
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString()
        ]);
        return redirect('/menu')->with('success','Success Insert Data !');
    }

    
    public function edit(Request $request,$id)
    {    
        $request->user()->authorizeRoles(['restaurant']);
        $data = DB::table('menu')->where('id',$id)->first();
        return view('menu.edit')->with('data',$data);
    }

    
    public function update(Request $request,$id)
    {    
        $request->user()->authorizeRoles(['restaurant']);
        $data = DB::table('menu')->where('id',$id)->first();
        if($request->file('banner') != null){
            Storage::delete('/menu//'.$request->user()->id.'/'.$data->image);
            $storagePath = Storage::put('/menu//'.$request->user()->id.'/',  $request->file('banner'));
            $storageName = basename($storagePath);
        }else{
            $storageName = $data->image;
        }
        
        DB::table('menu')->where('id',$id)->update([
            'rest_id'=>$request->user()->id,
            'name'=>$request['name'],
            'image'=>$storageName,
            'price'=>$request['price'],
            'description'=>$request['description'],
            'updated_at'=>Carbon::now()->toDateTimeString()
        ]);

        return redirect('/menu')->with('success','Success Update Data !');
    }

    public function delete(Request $request,$id)
    {    
        $request->user()->authorizeRoles(['restaurant']);
        
        $data = DB::table('menu')->where('id',$id)->first();

        // var_dump($data->image);
        // exit();

        Storage::delete('/menu//'.$request->user()->id.'/'.$data->image);
        DB::table('menu')->where('id',$id)->delete();
        return redirect('/menu')->with('success','Success Delete Data !');
    }
}
