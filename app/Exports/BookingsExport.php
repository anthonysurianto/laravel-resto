<?php

namespace App\Exports;

use App\User;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;

class BookingsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function view(): View
    {
        // return User::all();
        return view('excel.reportBooking', [
            'data' => DB::select('
                        select 
                            a.*,
                            case 
                                when a.manual_username IS NULL then b.name 
                                else a.manual_username
                            end as username,
                            case 
                                when a.manual_userphone IS NULL then b.phone 
                                else a.manual_userphone
                            end as userphone
                        from booking a
                        left join users b on a.user_id = b.id
                        where a.rest_id = :id
                ',['id'=> $this->id])
        ]);
    }
}
