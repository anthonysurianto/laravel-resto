<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','HomeController@index')->name('home');
Route::post('/search-booking','HomeController@searchBookByBookingId')->name('search-booking');
Route::get('/register-resto','Auth\RegisterController@registerResto')->name('register-resto');
Route::get('/resto','HomeController@viewResto')->name('resto');
Route::get('/resto/{id}','HomeController@viewRestoDetail')->name('resto-detail');

//Boooking
Route::get('booking','BookingController@index')->name('booking');
Route::get('booking/add','BookingController@add')->name('add-booking');
Route::post('booking/create','BookingController@create')->name('create-booking');
Route::get('accept-booking/{id}/{status}/{receiver}','BookingController@statusBooking')->name('status-booking');
Route::get('excel-booking','BookingController@downloadExcelBooking')->name('excel-booking');

// Manual Booking
Route::get('manual-booking/add','ManualBookingController@add')->name('add-manual-booking');
Route::post('manual-booking/create','ManualBookingController@create')->name('create-manual-booking');



// Manage Menu
Route::get('/menu','MenuController@index')->name('list-menu');
Route::get('/add-menu','MenuController@add')->name('add-menu');
Route::post('/create-menu','MenuController@insert')->name('insert-menu');
Route::get('/edit-menu/{id}','MenuController@edit')->name('edit-menu');
Route::post('/update-menu/{id}','MenuController@update')->name('update-menu');
Route::get('/delete-menu/{id}','MenuController@delete')->name('delete-menu');

Route::get('add-form','HomeController@addForm')->name('add-form');
Route::get('detail-form/{id}','HomeController@detailForm')->name('detail-form');
Route::get('history-form','HomeController@historyForm')->name('history-form');

Route::get('accept-form/{id}/{status}/{type_form}','HomeController@acceptForm')->name('accept-form');
Route::post('update-accept-form/{id}/{status}/{type_form}','HomeController@updateAcceptForm')->name('update-accept-form');
Route::get('update-reject-form/{id}/{status}/{type_form}','HomeController@updateRejectForm')->name('update-reject-form');
Route::get('report-peminjaman-form','HomeController@excelReportPeminjamanForm')->name('report-peminjaman-form');
Route::post('save-form','HomeController@saveForm')->name('save-form');

// Edit Profile
Route::get('profile/{id}','HomeController@detailProfile')->name('detail-profile');
Route::get('edit-profile/{id}','HomeController@editProfile')->name('edit-profile');
Route::post('profile/{id}','HomeController@updateProfile')->name('update-profile');

// Form Pengunduran Diri
Route::get('add-pengunduran-diri-form','FormPengunduranDiriController@addForm')->name('add-pengunduran-diri-form');
Route::get('detail-pengunduran-diri-form/{id}','FormPengunduranDiriController@detailForm')->name('detail-pengunduran-diri-form');
Route::post('save-pengunduran-diri-form','FormPengunduranDiriController@saveForm')->name('save-pengunduran-diri-form');

// Simpanan 
Route::get('list-simpanan','SimpananController@index')->name('list-simpanan');
Route::get('add-simpanan','SimpananController@add')->name('add-simpanan');
Route::get('edit-simpanan/{id}','SimpananController@edit')->name('edit-simpanan');
Route::post('save-simpanan','SimpananController@save')->name('save-simpanan');
Route::post('update-simpanan/{id}','SimpananController@update')->name('update-simpanan');

// Messages
Route::get('/messages','MessageController@index')->name('message');

Route::get('/admin/user','UserController@viewUser')->name('admin-list-user');
Route::get('/admin/user-status/{id}/{active}','UserController@updateUserStatus')->name('admin-update-user-status');
Route::get('/admin/user-role/{id}/{role_id}','UserController@updateUserRole')->name('admin-update-user-role');

// Admin Messages
Route::get('/admin/messages','MessageController@add')->name('add-message');
Route::post('/admin/messages','MessageController@insert')->name('insert-message');
Route::get('/admin/messages/{id}','MessageController@delete')->name('delete-message');


// Admin Banner
Route::get('/admin/banner','AdminController@indexBanner')->name('admin-list-banner');
Route::get('/admin/add-banner','AdminController@addBanner')->name('admin-add-banner');
Route::post('/admin/banner','AdminController@insertBanner')->name('admin-insert-banner');
Route::get('/admin/edit-banner/{id}','AdminController@editBanner')->name('admin-edit-banner');
Route::post('/admin/banner/{id}','AdminController@updateBanner')->name('admin-update-banner');
Route::get('/admin/banner/{id}','AdminController@deleteBanner')->name('admin-delete-banner');

Auth::routes();