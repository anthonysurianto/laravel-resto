@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-10">
                            Menu
                        </div>
                        @if(Auth::user()->hasAnyRole(['restaurant']))
                        <div class="col-2">
                            <a class="float-right"  href="{{ route('add-menu') }}">Add Menu</a>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                    @endif

                    @if (session('invalid'))
                        <div class="alert alert-danger">
                            {{session('invalid')}}
                        </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-md-12">
                            @if(Auth::user()->hasAnyRole(['restaurant']))
                            <label for="name" class="col-md-4 col-form-label text-md-center">Menu</label>
                            <label for="name" class="col-md-2 col-form-label text-md-center">Name</label>
                            <label for="name" class="col-md-2 col-form-label text-md-center">Price</label>
                            <label for="name" class="col-md-2 col-form-label text-md-center">Description</label>
                            @endif
                        </div>
                    </div>
                    @foreach($data as $record)
                    <div class="form-group row">
                        <div class="col-md-12">
                            @if(Auth::user()->hasAnyRole(['restaurant']))
                            <label for="name" class="col-md-4 col-form-label text-md-center">    
                                <img src="{{asset('image/menu/'.$record->rest_id.'/'.$record->image)}}" alt="Smiley face" style="width:100%!important;display:inline-block !important;">
                            </label>
                            <label for="name" class="col-md-2 col-form-label text-md-center">    
                                {{$record->name}}
                            </label>
                            <label for="name" class="col-md-2 col-form-label text-md-center">    
                                {{$record->price}}
                            </label>
                            <label for="name" class="col-md-2 col-form-label text-md-center">    
                                {{$record->description}}
                            </label>
                            <label for="name" class="col-form-label"><a href="{{ route('edit-menu',['id'=>$record->id]) }}">Edit</a></label>
                            <label for="name" class="col-form-label text-center">|</label>
                            <label for="name" class="col-form-label text-md-right"><a href="{{ route('delete-menu',['id'=>$record->id]) }}">Delete</a></label>
                            @endif   
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
