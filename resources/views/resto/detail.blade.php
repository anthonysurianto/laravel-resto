@extends('layouts.app')

@section('content')
<div class="data-container">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-10">
                               {{$data->resto->name}} - {{$data->resto->category_name}}
                            </div>
                            <div class="col-2">
                                Open : {{substr($data->resto->rest_open, 0, -3)}} - {{substr($data->resto->rest_closed, 0, -3)}}
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12">                                                                
                                @if(count($data->menu) > 0)
                                    @foreach($data->menu as $record)
                                    <div class="data-container">
                                        <div class="container">
                                            <div class="row justify-content-center">
                                                <div class="col-md-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <div class="row">
                                                                <div class="col-10">
                                                                    {{$record->name}}
                                                                </div>
                                                                <div class="col-2">
                                                                    Rp.{{ number_format($record->price,2,',','.') }}
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="card-body">
                                                            <div class="form-group row">
                                                                <div class="col-md-12">    
                                                                    <label for="name" class="col-md-4 col-form-label text-md-center">    
                                                                        <img src="{{asset('image/menu/'.$record->rest_id.'/'.$record->image)}}" alt="Smiley face" style="width:100%!important;display:inline-block !important;">
                                                                    </label>
                                                                    <label for="name" class="col-4 col-form-label text-md-center">{{$record->description}}</label>     
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
