@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('message'))
        <div class="alert alert-success">
            {{session('message')}}
        </div>
    @endif
    <form method="POST" action="{{ route('search-booking') }}">
        @csrf
        <div class="form-group row">
            <div class="col-md-10">
                <input id="booking_id" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="booking_id" placeholder="Input Booking Id.." required autofocus>
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">
                    Search
                </button>
            </div>
        </div>
    </form>

    @if(count($data)>0)
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-10">
                            List Booking
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-md-12">
                            @if(Auth::user()->hasAnyRole(['restaurant']))
                                <label for="name" class="col-2 col-form-label text-md-center">Booking Id</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Name</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Phone</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Date Book</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Status</label>
                            @endif
                        </div>
                    </div>
                    @foreach($data as $record)
                    <div class="form-group row">
                        <div class="col-md-12">
                            @if(Auth::user()->hasAnyRole(['restaurant']))   
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->booking_id}}</label>                       
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->username}}</label>                     
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->userphone}}</label>  
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->date_book}} {{substr($record->time_book, 0, -3)}}</label>   
                                <label for="name" class="col-2 col-form-label text-md-center">
                                    @if($record->status == 0)
                                        Rejected
                                    @endif
                                    @if($record->status == 1)
                                        <a href="{{ route('status-booking',['id'=>$record->id,'status'=>2, 'receiver' => $record->user_id ]) }}">Accept</a> | 
                                        <a href="{{ route('status-booking',['id'=>$record->id,'status'=>0, 'receiver' => $record->user_id ]) }}">Reject</a>
                                    @endif
                                    @if($record->status == 2)
                                        <a href="{{ route('status-booking',['id'=>$record->id,'status'=>3, 'receiver' => $record->user_id ]) }}">Customer Come</a> | 
                                        <a href="{{ route('status-booking',['id'=>$record->id,'status'=>4, 'receiver' => $record->user_id ]) }}">Customer Not Come</a>
                                    @endif
                                    @if($record->status == 3)
                                        Customer has come to the restaurant
                                    @endif
                                    @if($record->status == 4)
                                        Customer has not come to the restaurant
                                    @endif
                                </label>   
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
