@extends('layouts.app')

@section('content')
<div class="data-container">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10">
                <form method="GET" action="{{ route('resto') }}">
                    <input class="form-control" type="text" name="search_resto" value="{{ app('request')->input('search_resto') == null ? null : app('request')->input('search_resto')}}" placeholder="Search" aria-label="Search">
                </form>
            </div>
        </div>
    </div>
</div>
@foreach($data as $record)
<div class="data-container">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-10">
                                <a href="{{ route('resto-detail',['id'=> $record->id]) }}" target="_blank">{{$record->name}} - {{$record->category_name}}</a>
                            </div>
                            <div class="col-2">
                            Open : {{substr($record->rest_open, 0, -3)}} - {{substr($record->rest_closed, 0, -3)}}
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="name" class="col-4 col-form-label text-md-center">{{$record->rest_desc}}</label>     
                            </div>
                            @if(Auth::user())
                                @if(Auth::user()->hasAnyRole(['user']))
                                    <div class="col-md-12">
                                        <label for="name" class="col-4 offset-8 col-form-label text-right"><a href="{{ route('add-booking',['resto_id'=> $record->id]) }}">Book Now</a></label>     
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                    
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-12 text-right">
                                <label>
                                    Phone / HP : {{$record->phone_home}} / {{$record->phone}}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection
