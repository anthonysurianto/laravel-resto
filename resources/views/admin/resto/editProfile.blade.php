@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Profile
                </div>
                <div class="card-body">
                    @if (session('invalid'))
                        <div class="alert alert-danger">
                            {{session('invalid')}}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('update-profile',['id'=>$data->resto->id]) }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $data->resto->name }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $data->resto->email }}" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="current_password" class="col-md-4 col-form-label text-md-right">Current Password</label>
                            <div class="col-md-6">
                                <input id="current_password" type="password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" name="current_password" value="">
                                @if ($errors->has('current_password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('current_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">Password Confirmation</label>
                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" value="">
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="dop" class="col-md-4 col-form-label text-md-right">Tempat / Tanggal Lahir</label>
                            <div class="col-md-6">
                                <input id="dop" type="text" class="col-md-4 form-control{{ $errors->has('dop') ? ' is-invalid' : '' }}" name="dop" value="{{ $data->resto->dop }}" required autofocus>
                                <input id="dob" type="date" class="col-md-8 form-control{{ $errors->has('dob') ? ' is-invalid' : '' }}" name="dob" value="{{ $data->resto->dob }}" required autofocus>
                                @if ($errors->has('dop'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('dop') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('dob'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Alamat</label>
                            <div class="col-md-6">
                                <textarea id="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ $data->resto->address }}" required autofocus>{{$data->resto->address}}</textarea>
                                @if ($errors->has('address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone_home" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                            <div class="col-md-6">
                                <input id="phone_home" type="text" class="form-control{{ $errors->has('phone_home') ? ' is-invalid' : '' }}" name="phone_home" value="{{ $data->resto->phone_home }}" required autofocus>
                                @if ($errors->has('phone_home'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone_home') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">HP</label>
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ $data->resto->phone }}" required autofocus>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Restaurant Category</label>
                            <div class="col-md-6">
                                <select id="rest_category"  class="form-control{{ $errors->has('rest_category') ? ' is-invalid' : '' }}" name="rest_category">
                                    @foreach($data->resto_category as $key => $value)
                                        <option value="{{$value->id}}" {{ $value->id == $data->resto->rest_category_id ? 'selected' : '' }} >{{$value->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Restaurant Desc</label>
                            <div class="col-md-6">
                            <textarea id="rest_desc" class="form-control{{ $errors->has('rest_desc') ? ' is-invalid' : '' }}" name="rest_desc" value="{{ $data->resto->rest_desc }}" required autofocus>{{$data->resto->rest_desc}}</textarea>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="rest_open" class="col-md-4 col-form-label text-md-right">Restaurant Open</label>
                            <div class="col-md-6">
                                <input id="rest_open" type="time" class="form-control{{ $errors->has('rest_open') ? ' is-invalid' : '' }}" name="rest_open" value="{{ $data->resto->rest_open }}" required autofocus>
                                @if ($errors->has('rest_open'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('rest_open') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="rest_closed" class="col-md-4 col-form-label text-md-right">Restaurant Closed</label>
                            <div class="col-md-6">
                                <input id="rest_closed" type="time" class="form-control{{ $errors->has('rest_closed') ? ' is-invalid' : '' }}" name="rest_closed" value="{{ $data->resto->rest_closed }}" required autofocus>
                                @if ($errors->has('rest_closed'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('rest_closed') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>                                
                                <button class="btn btn-primary">
                                    <a href="{{ route('detail-profile',['id'=>$data->resto->id])}}">back</a>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
