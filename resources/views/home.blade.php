@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('message'))
        <div class="alert alert-success">
            {{session('message')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12 slide-content">
            @foreach($data->banner as $value)
                <img class="slider" src="{{asset('image/'.$value)}}" alt="Smiley face" style="width:100%!important;">
            @endforeach                
            <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
            <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <a href="{{ route('resto') }}">
                View Restaurant
            </a>
        </div>
        <div class="col-md-4 col-sm-12 text-center">
            New This Week
        </div>
        <div class="col-md-3 offset-md-1 col-sm-12 text-right">
            <form method="GET" action="{{ route('resto') }}">
                <input class="form-control" type="text" name="search_resto" placeholder="Search" aria-label="Search">
            </form>
        </div>
    </div>
    <div class="row p-3">
        @foreach($data->menu as $key=>$value)
            <div class="col-md-4 col-sm-12">
                <div class="row">
                    @if($key==1)
                    <div class="col-md-11 border border-dark" style="margin-left:4%;">
                    @else
                    <div class="col-md-11 {{$key==2 ?'offset-md-1':''}} border border-dark">
                    @endif
                        <div class="row">
                            <div class="col-12 text-center">
                                <label>
                                    <a href="{{ route('resto-detail',['id'=> $value->rest_id]) }}" target="_blank">
                                        {{$value->rest_name}}
                                    </a>
                                </label>
                            </div>
                            <div class="col-6">
                                <img src="{{asset('image/menu/'.$value->rest_id.'/'.$value->image)}}" alt="Smiley face" style="width:100%!important;display:inline-block !important;padding-bottom:10px;">
                            </div>
                            <div class="col-6">
                                <label>
                                    {{$value->name}}
                                </label>
                                <label>
                                    {{$value->description}}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        @endforeach     
    </div>
</div>
@endsection
