<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Booking Id</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Date Book</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key=>$value)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $value->booking_id }}</td>
            <td>{{ $value->username }}</td>
            <td>{{ $value->userphone }}</td>
            <td>{{$value->date_book}} {{substr($value->time_book, 0, -3)}}</td>
            <td>
            @if($value->status == 0)
                Rejected
            @endif
            @if($value->status == 1)
                Wait to respond
            @endif
            @if($value->status == 2)
                Accepted
            @endif
            @if($value->status == 3)
                Customer has come to the restaurant
            @endif
            @if($value->status == 4)
                Customer has not come to the restaurant
            @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>