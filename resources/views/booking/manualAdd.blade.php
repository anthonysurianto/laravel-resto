@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Manual Booked Restaurant Form</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('create-manual-booking') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="rest_name" class="col-md-4 col-form-label text-md-right">Restaurant Name : </label>
                            <label for="rest_name" class="col-md-6 col-form-label"> {{ Auth::user()->name }} </label>
                        </div>
                        
                        <div class="form-group row">
                            <label for="manual_username" class="col-md-4 col-form-label text-md-right">Booked Name : </label>
                            
                            <div class="col-md-6">
                                <input id="manual_username" type="text" class="form-control{{ $errors->has('manual_username') ? ' is-invalid' : '' }}" name="manual_username" value="{{ old('manual_username') }}" required autofocus>
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="manual_userphone" class="col-md-4 col-form-label text-md-right">Booked Phone : </label>
                            
                            <div class="col-md-6">
                                <input id="manual_userphone" type="number" class="form-control{{ $errors->has('manual_userphone') ? ' is-invalid' : '' }}" name="manual_userphone" value="{{ old('manual_userphone') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date_book" class="col-md-4 col-form-label text-md-right">Date Book</label>

                            <div class="col-md-6">
                                <input id="date_book" type="date" class="form-control{{ $errors->has('date_book') ? ' is-invalid' : '' }}" name="date_book" value="{{ old('date_book') }}" min="{{date('Y-m-d')}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="time_book" class="col-md-4 col-form-label text-md-right">Time Book</label>

                            <div class="col-md-6">
                                <input id="time_book" type="time" class="form-control{{ $errors->has('time_book') ? ' is-invalid' : '' }}" name="time_book" value="{{ old('date_book') }}" min="{{$data->resto->rest_open}}"  max="22:00" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="qty" class="col-md-4 col-form-label text-md-right">Quantity : </label>
                            <div class="col-md-6">
                                <input id="qty" type="number" class="form-control{{ $errors->has('qty') ? ' is-invalid' : '' }}" name="qty" value="{{ old('qty') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
