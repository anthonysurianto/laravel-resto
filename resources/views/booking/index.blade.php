@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-10">
                            List Booking
                        </div>
                        
                        @if(Auth::user()->hasAnyRole(['restaurant']))
                        <div class="col-2">
                            <a href="{{ route('excel-booking') }}">Download</a>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-md-12">
                            @if(Auth::user()->hasAnyRole(['user']))
                                <label for="name" class="col-2 col-form-label text-md-center">Booking Id</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Restaurant Name</label>
                                <label for="name" class="col-3 col-form-label text-md-center">Restaurant Phone</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Date Book</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Status</label>
                            @endif
                            @if(Auth::user()->hasAnyRole(['restaurant']))
                                <label for="name" class="col-2 col-form-label text-md-center">Booking Id</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Name</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Phone</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Date Book</label>
                                <label for="name" class="col-1 col-form-label text-md-center">Quantity</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Status</label>
                            @endif
                            @if(Auth::user()->hasAnyRole(['admin']))
                                <label for="name" class="col-1 col-form-label text-md-center">Booking Id</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Name</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Phone</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Restaurant Name</label>
                                <label for="name" class="col-2 col-form-label text-md-center">Date Book</label>
                                <label for="name" class="col-1 col-form-label text-md-center">Quantity</label>
                                <label for="name" class="col-1 col-form-label text-md-center">Status</label>
                            @endif
                        </div>
                    </div>
                    @foreach($data as $record)
                    <div class="form-group row"  style="font-size:small;">
                        <div class="col-md-12">
                            @if(Auth::user()->hasAnyRole(['user']))                        
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->booking_id}}</label>        
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->rest_name}}</label>   
                                <label for="name" class="col-3 col-form-label text-md-center">{{$record->rest_phone_home}} / {{$record->rest_phone}}</label>  
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->date_book}} {{substr($record->time_book, 0, -3)}}</label>      
                                <label for="name" class="col-2 col-form-label text-md-center">
                                    @if($record->status == 0)
                                        Rejected
                                    @endif
                                    @if($record->status == 1)
                                        Wait to respond
                                    @endif
                                    @if($record->status == 2)
                                        Accepted
                                    @endif
                                    @if($record->status == 3 || $record->status == 4)
                                        Finished
                                    @endif
                                </label>           
                            @endif
                            @if(Auth::user()->hasAnyRole(['restaurant']))   
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->booking_id}}</label>                       
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->username}}</label>                     
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->userphone}}</label>  
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->date_book}} {{substr($record->time_book, 0, -3)}}</label>                    
                                <label for="name" class="col-1 col-form-label text-md-center">{{$record->qty}}</label>  
                                <label for="name" class="col-2 col-form-label text-md-center">
                                    @if($record->status == 0)
                                        Rejected
                                    @endif
                                    @if($record->status == 1)
                                        <a href="{{ route('status-booking',['id'=>$record->id,'status'=>2, 'receiver' => $record->user_id ]) }}">Accept</a> | 
                                        <a href="{{ route('status-booking',['id'=>$record->id,'status'=>0, 'receiver' => $record->user_id ]) }}">Reject</a>
                                    @endif
                                    @if($record->status == 2)
                                        <a href="{{ route('status-booking',['id'=>$record->id,'status'=>3, 'receiver' => $record->user_id ]) }}">Customer Come</a> | 
                                        <a href="{{ route('status-booking',['id'=>$record->id,'status'=>4, 'receiver' => $record->user_id ]) }}">Customer Not Come</a>
                                    @endif
                                    @if($record->status == 3)
                                        Customer has come to the restaurant
                                    @endif
                                    @if($record->status == 4)
                                        Customer has not come to the restaurant
                                    @endif
                                </label>   
                            @endif
                            @if(Auth::user()->hasAnyRole(['admin']))   
                                <label for="name" class="col-1 col-form-label text-md-center">{{$record->booking_id}}</label>                       
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->username}}</label>                     
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->userphone}}</label>                    
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->rest_name}}</label>  
                                <label for="name" class="col-2 col-form-label text-md-center">{{$record->date_book}} {{substr($record->time_book, 0, -3)}}</label>                    
                                <label for="name" class="col-1 col-form-label text-md-center">{{$record->qty}}</label>     
                                <label for="name" class="col-1 col-form-label text-md-center">
                                    @if($record->status == 0)
                                        Rejected
                                    @endif
                                    @if($record->status == 1)
                                        Wait to respond
                                    @endif
                                    @if($record->status == 2)
                                        Accepted
                                    @endif
                                    @if($record->status == 3)
                                        Customer has come to the restaurant
                                    @endif
                                    @if($record->status == 4)
                                        Customer has not come to the restaurant
                                    @endif
                                </label>   
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
