@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Booked Restaurant Form</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('create-booking') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="rest_name" class="col-md-4 col-form-label text-md-right">Restaurant Name : </label>
                            <label for="rest_name" class="col-md-6 col-form-label text-left">{{$data->resto->name}}</label>
                            <input id="resto_id" type="text" class="form-control d-none" name="rest_id" value="{{$data->resto->id}}">
                            <input id="receiver" type="text" class="form-control d-none" name="receiver" value="{{$data->resto->id}}">
                        </div>

                        <div class="form-group row">
                            <label for="date_book" class="col-md-4 col-form-label text-md-right">Date Book</label>

                            <div class="col-md-6">
                                <input id="date_book" type="date" class="form-control{{ $errors->has('date_book') ? ' is-invalid' : '' }}" name="date_book" value="{{ old('date_book') }}" min="{{date('Y-m-d')}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="time_book" class="col-md-4 col-form-label text-md-right">Time Book</label>

                            <div class="col-md-6">
                                <input id="time_book" type="time" class="form-control{{ $errors->has('time_book') ? ' is-invalid' : '' }}" name="time_book" value="{{ old('date_book') }}" min="{{$data->resto->rest_open}}"  max="22:00" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="qty" class="col-md-4 col-form-label text-md-right">Quantity : </label>
                            <div class="col-md-6">
                            <input id="qty" type="number" class="form-control{{ $errors->has('qty') ? ' is-invalid' : '' }}" name="qty" value="{{ old('qty') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
