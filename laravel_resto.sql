-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2019 at 05:31 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_resto`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, '9JNjxgFr6QXeDCnnZN5PXIUp5pf40EqTW4uR3JlB.jpeg', '2018-08-19 04:36:39', '2018-08-18 21:36:39'),
(5, 'SX9vrVE9G0KEAJIR6apsqskkKQuUr738BL9vRTJo.jpeg', '2018-08-18 20:36:44', '2018-08-18 20:36:44'),
(6, 'IQvKPL6GkVlTafK3HXjw1KiA0I0IP9mnUKblmZ5d.jpeg', '2018-11-08 01:07:06', '2018-11-08 01:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `booking_id` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `manual_username` varchar(100) DEFAULT NULL,
  `manual_userphone` varchar(100) DEFAULT NULL,
  `date_book` date NOT NULL,
  `time_book` time NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `booking_id`, `user_id`, `rest_id`, `manual_username`, `manual_userphone`, `date_book`, `time_book`, `qty`, `status`, `created_at`, `updated_at`) VALUES
(2, 'R400002', 1, 4, NULL, NULL, '2019-04-06', '10:20:00', 0, 2, '2019-04-05 03:19:15', '2019-04-08 07:56:02'),
(3, 'R400003', 1, 4, NULL, NULL, '2019-04-24', '18:00:00', 10, 1, '2019-04-24 08:11:56', '2019-04-24 08:11:56'),
(4, 'R400004', 0, 4, 'ASDFFGGGG', '081355559977', '2019-04-25', '15:00:00', 11, 1, '2019-04-25 06:28:43', '2019-04-25 06:28:43');

-- --------------------------------------------------------

--
-- Table structure for table `form_dana`
--

CREATE TABLE `form_dana` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type_peminjaman_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `dana` int(11) NOT NULL,
  `dana_awal` int(11) NOT NULL,
  `dana_potongan` int(11) NOT NULL,
  `terbilang` varchar(100) NOT NULL,
  `keperluan` varchar(100) NOT NULL,
  `cicilan` int(11) NOT NULL,
  `cicilan_potongan` int(11) NOT NULL,
  `tanggal_dana` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_dana`
--

INSERT INTO `form_dana` (`id`, `user_id`, `type_peminjaman_id`, `name`, `dana`, `dana_awal`, `dana_potongan`, `terbilang`, `keperluan`, `cicilan`, `cicilan_potongan`, `tanggal_dana`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Form 1', 10000000, 11000000, 0, 'Sepuluh Juta Rupiah', 'Dana kebutuhan', 12, 0, '2018-04-11', 1, '2018-11-08 08:38:58', '0000-00-00 00:00:00'),
(2, 1, 1, 'Form 121212', 10000000, 12000000, 10000000, 'Uang somtehing', 'keprluan dadakan', 9, 9, '2018-03-20', 0, '2018-11-08 08:39:01', '2018-04-23 18:20:55'),
(4, 1, 2, 'ASDJKHDKJ', 120000, 120000, 120000, 'ASDJIUADHIDHA', 'qweqnuwiehqbiewbiu', 12, 12, '2018-09-30', 1, '2018-11-08 08:37:59', '2018-08-09 01:41:16'),
(5, 1, 1, 'ASJDKSAJDL', 1202020, 1202020, 1202020, 'jwqekoqwe', 'jqwoeiqieo', 10, 10, '2019-03-31', 0, '2019-02-05 05:40:56', '2019-02-05 05:40:56');

-- --------------------------------------------------------

--
-- Table structure for table `form_pengunduran_diri`
--

CREATE TABLE `form_pengunduran_diri` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `alamat` varchar(99) NOT NULL,
  `alasan` varchar(99) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_pengunduran_diri`
--

INSERT INTO `form_pengunduran_diri` (`id`, `user_id`, `name`, `alamat`, `alasan`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'ASDFGGGGG', 'ALAMAT SAYA', 'iwqopeiqpeipqwe', 1, '2018-07-02 06:06:09', '2018-06-01 06:03:19');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `rest_id`, `name`, `image`, `description`, `price`, `created_at`, `updated_at`) VALUES
(1, 4, 'Menu 1', '3Z6dvOqckPuOBCYCw7aQjk1ikTxj0QnQFmHLrsci.jpeg', 'Description Menu 1', 10000, '2019-04-10 03:29:09', '2019-04-09 20:29:09'),
(2, 4, 'Menu 2', '8D1GVcOH3GWpX4L8KezJ6X3v8RsrfzrWokihYMa5.jpeg', 'Description Menu 2', 8000, '2019-04-10 03:28:33', '2019-04-09 20:28:33'),
(4, 4, 'qwewqewqewqe', 'kgI1nWDUaIId8Up45NgwlWhT4KLkSHSLvcjv4nYw.jpeg', 'BJASDBHJABDHASJKDSA', 100000, '2019-04-10 03:28:56', '2019-04-09 20:28:56');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `message` varchar(300) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `receiver_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sudah di transfer sebesar XXXXX', '2018-05-08 08:43:22', '2018-05-08 08:43:22'),
(5, 1, 'ASDASDASDAS', '2018-05-09 02:21:00', '2018-05-09 02:21:00'),
(8, 1, 'XAXAXAXAAXAAAAAAA', '2018-05-09 02:47:19', '2018-05-09 02:47:19'),
(10, 1, 'DDDDDDD', '2018-05-09 02:49:32', '2018-05-09 02:49:32'),
(11, 1, 'AFAFAFAFAAFA', '2018-05-09 02:49:36', '2018-05-09 02:49:36'),
(12, 1, 'Pengajuan peminjaman ada sudah di setujui, dana akan di trf pada tgl: 2018-08-16', '2018-06-16 16:48:02', '2018-06-16 16:48:02'),
(13, 1, 'Pengajuan pengunduran diri sudah di setujui', '2018-07-02 05:48:08', '2018-07-02 05:48:08'),
(14, 1, 'Pengajuan pengunduran diri sudah di setujui', '2018-07-02 06:01:30', '2018-07-02 06:01:30'),
(15, 1, 'Pengajuan pengunduran diri sudah di setujui', '2018-07-02 06:06:09', '2018-07-02 06:06:09'),
(16, 1, 'Pengajuan peminjaman dana sudah di setujui, dana akan di trf pada tgl: 1994-03-20', '2018-08-02 13:58:45', '2018-08-02 13:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_10_030110_create_roles_table', 1),
(4, '2018_04_10_061749_create_role_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `user_id`, `count`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '2018-05-09 00:00:00', '2018-05-09 09:09:10'),
(2, 4, 1, '2019-04-05 03:19:15', '2019-04-05 03:19:15');

-- --------------------------------------------------------

--
-- Table structure for table `notification_messages`
--

CREATE TABLE `notification_messages` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_messages`
--

INSERT INTO `notification_messages` (`id`, `sender_id`, `receiver_id`, `message`, `created_at`, `updated_at`) VALUES
(2, 1, 4, 'User Name is want to book at your restaurant', '2019-04-08 06:55:23', '2019-04-04 20:19:15'),
(3, 4, 1, 'Resto 1 accept your booking !', '2019-04-08 00:56:02', '2019-04-08 00:56:02'),
(4, 1, 4, 'User Name is want to book at your restaurant', '2019-04-24 01:11:56', '2019-04-24 01:11:56');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resto_category`
--

CREATE TABLE `resto_category` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resto_category`
--

INSERT INTO `resto_category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'eastern', '2019-04-08 02:54:15', '2019-04-08 02:54:15'),
(2, 'western', '2019-04-08 02:54:15', '2019-04-08 02:54:15');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user', 'An User', '2018-04-09 23:35:32', '2018-04-09 23:35:32'),
(2, 'admin', 'An admin user', '2018-04-09 23:35:32', '2018-04-09 23:35:32'),
(3, 'restaurant', 'User Restoran', '2019-03-31 17:00:00', '2019-03-31 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(4, 3, 4),
(5, 3, 6),
(6, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `simpanan`
--

CREATE TABLE `simpanan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dana` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `simpanan`
--

INSERT INTO `simpanan` (`id`, `user_id`, `dana`, `created_at`, `updated_at`) VALUES
(1, 1, 300000, '2018-08-14 00:55:23', '2018-08-14 05:58:13'),
(2, 4, 100000, '2018-08-14 04:03:27', '2018-08-14 05:58:51');

-- --------------------------------------------------------

--
-- Table structure for table `type_peminjaman`
--

CREATE TABLE `type_peminjaman` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `limit_peminjaman` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_peminjaman`
--

INSERT INTO `type_peminjaman` (`id`, `name`, `limit_peminjaman`, `created_at`, `updated_at`) VALUES
(1, 'regular', 5000000, '2018-07-28 08:19:02', '2018-07-28 08:19:02'),
(2, 'barang', 3000000, '2018-07-28 08:19:02', '2018-07-28 08:19:02'),
(3, 'sembako', 5000000, '2018-07-28 08:19:12', '2018-07-28 08:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dop` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_home` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rest_category_id` int(11) NOT NULL DEFAULT '0',
  `rest_desc` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rest_open` time DEFAULT NULL,
  `rest_closed` time DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `dop`, `dob`, `address`, `phone_home`, `phone`, `rest_category_id`, `rest_desc`, `rest_open`, `rest_closed`, `email`, `password`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'User Name', 'Jakarta', '2012-12-12', 'qekqojwejqow', '13213132132', '123132132132', 0, '', '00:00:00', '00:00:00', 'user@mail.com', '$2y$10$j0hriM..sWFme7OUufVQbuOHZwPajA/mADOesvCgTPjUeA3IpWV0K', 1, 'L15PdDhXeETCtMLXkt1YyZoRNuZysmAmSlrKMWcwTrZBTU0J4cTMDyf5qyo9', '2018-04-09 23:35:32', '2018-11-04 20:57:39'),
(2, 'Admin Name', '', '0000-00-00', '', '', '', 0, '', '00:00:00', '00:00:00', 'admin@mail.com', '$2y$10$6XJ9yh.LQKyocmOWqOSgDum/EUzFfeN.QwnvnMK6l9u3NP5NLfddG', 1, 'HF7fNGR4NJms8P406nINZk9SjiwdL0iwkjd8gG5qc7R6fq42DcYYhpMtENMA', '2018-04-09 23:35:32', '2018-11-04 20:58:19'),
(4, 'Resto 1', 'Jakarta', '1990-09-16', 'ASNDKLJADJLAD', '021 890182801', '08123732737', 1, '', '10:00:00', '22:00:00', 'resto@mail.com', '$2y$10$cBdPalcmbwA7Nuelo0Yjcu.87IFGIbFUbAR9xfXf3WQ/AElLzzJAm', 1, 'K5QNclEflxe83QMoNy5XJKjjriG8Y6WQOV57Lu35wbUHUsI1YfGmGj65VRrc', '2018-05-06 00:19:54', '2018-11-04 20:58:29'),
(6, 'Resto 2', 'Jakarta', '1990-09-16', 'ASNDKLJADJLAD', '021 890182801', '08123732737', 2, '', '10:00:00', '22:00:00', 'resto2@mail.com', '$2y$10$cBdPalcmbwA7Nuelo0Yjcu.87IFGIbFUbAR9xfXf3WQ/AElLzzJAm', 1, 'nh6kFdgNIcnLtJ4357l6XJnWRwDB3cCBuq6Z9XuWeDLEzN4RoeTGEYuUweMk', '2018-05-06 00:19:54', '2018-11-04 20:58:29'),
(7, 'user5', 'Jakarta', '1990-01-01', 'ASDFFFFF', '45569777', '081354477899', 0, NULL, NULL, NULL, 'user5@mail.com', '$2y$10$yrG43fnouJo8qMEy2ZJZUOahBe67ag4PM/WtcKXFNV/em1bor1sWa', 1, 'WdKHxNfvLou9gB43VXRATzkad004571yxTm1QjsDI4eOcHdMIg41ocWyxGUO', '2019-04-16 02:06:38', '2019-04-16 02:06:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_dana`
--
ALTER TABLE `form_dana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_pengunduran_diri`
--
ALTER TABLE `form_pengunduran_diri`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receiver_id` (`receiver_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `notification_messages`
--
ALTER TABLE `notification_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `resto_category`
--
ALTER TABLE `resto_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `simpanan`
--
ALTER TABLE `simpanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_peminjaman`
--
ALTER TABLE `type_peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `form_dana`
--
ALTER TABLE `form_dana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `form_pengunduran_diri`
--
ALTER TABLE `form_pengunduran_diri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notification_messages`
--
ALTER TABLE `notification_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `resto_category`
--
ALTER TABLE `resto_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `simpanan`
--
ALTER TABLE `simpanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `type_peminjaman`
--
ALTER TABLE `type_peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `form_pengunduran_diri`
--
ALTER TABLE `form_pengunduran_diri`
  ADD CONSTRAINT `form_pengunduran_diri_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
